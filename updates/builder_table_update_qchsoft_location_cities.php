<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftLocationCities extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->integer('external_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->dropColumn('external_id');
        });
    }
}
