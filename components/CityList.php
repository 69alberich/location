<?php namespace QchSoft\Location\Components;

use QchSoft\Location\Models\City;
use Cms\Classes\ComponentBase;
use Input;

class CityList extends ComponentBase{

    public function componentDetails(){
        return [
            'name'        => 'Display City List',
            'description' => 'methods for City list model',
        ];
    }

    public function defineProperties(){
        return [
            'search_parameter' => [
                'title' => 'Seach Parameter',
                'description' => 'search parameter',
                'type' => 'string',

            ],
            'scope' => [
                "title" => 'Scope',
                'description' => 'Scope for search',
                'type' => "dropdown",
                'options'     => [
                    'state'=>'By State',
                    'country'=>'By Country',
                ]
            ],
            'type' =>[
                "title" => "Type",
                "description" => "Scope type",
                'type' => "dropdown",
                'options'     => [
                    'code'=>'By code',
                    'id'=>'By id',
                ]
            ]
        ];
    }

    public function getCityList(){
        
        $properties = $this->getProperties();
        $functionName = camel_case($properties["scope"]."_".$properties["type"]);
        
        return City::{$functionName}([$properties["search_parameter"]])->get();
    }

    public function onSetCityIdPage(){
        $data = post();

        $this->page["cityId"] = $data["city"];

    }

}