<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftLocationCities4 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->string('external_id', 150)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->integer('external_id')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
