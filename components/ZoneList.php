<?php namespace QchSoft\Location\Components;

use QchSoft\Location\Models\Zone;
use Cms\Classes\ComponentBase;
use Input;

class ZoneList extends ComponentBase{

    public function componentDetails(){
        return [
            'name'        => 'Display Zone List',
            'description' => 'methods for zone list model',
        ];
    }

    public function defineProperties(){
        return [
            'search_parameter' => [
                'title' => 'Seach Parameter',
                'description' => 'search parameter',
                'type' => 'string',

            ],
            'scope' => [
                "title" => 'Scope',
                'description' => 'Scope for search',
                'type' => "dropdown",
                'options'     => [
                    'state'=>'By State',
                    'city'=>'By City',
                    'country'=>'By Country',
                ]
            ],
            'type' =>[
                "title" => "Type",
                "description" => "Scope type",
                'type' => "dropdown",
                'options'     => [
                    'code'=>'By code',
                    'id'=>'By id',
                ]
            ]
        ];
    }

    public function getZoneList($cityId = null){

        if(empty($cityId)){
            if (isset($properties["scope"]) && isset($properties["type"])) {
                $properties = $this->getProperties();
                $functionName = camel_case($properties["scope"]."_".$properties["type"]);
                return Zone::{$functionName}([$properties["search_parameter"]])->get();
             
            }else{
                return "no retorno nadita we";
            }
           
        }else{

            return Zone::cityId([$cityId])->get(); 
           
            
        }
        
    }
    

    /*
    public function onSentForm(){
        $data = post();
        trace_log($data);
        
        $emailList = explode(",", ShopaHolicSettings::get("creating_order_manager_email_list"));
        $form = array("form" => $data);
        
        try {
            Mail::send("qchsoft.yatchextension::mail.contact-form",
            $form, function($message) use ($emailList) {
            foreach ($emailList as $email) {
                $message->to($email);
            }
            //$message->to("alberich2052@gmail.com");
            
            });
            return Response::json(['success' => true]);
            
        } catch (\Throwable $th) {

            return Response::json(['success' => false]);

        }

        
    }
    */
    
}

