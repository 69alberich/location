<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftLocationCities extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_location_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->integer('state_id');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_location_cities');
    }
}
