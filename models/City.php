<?php namespace Qchsoft\Location\Models;

use Model;
use Qchsoft\Location\Models\State;
use Qchsoft\Location\Models\Zone;
/**
 * Model
 */
class City extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_location_cities';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'state' => State::class
    ];

    public $hasMany = [
        'zones' => Zone::class
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public $fillable = [
        'name',
        'code',
        'state_id',
        'description',
        'external_id',
        'active',
    ];


    public function scopeStateId($query, $arStateIds){
        
        return $query->whereIn('state_id', $arStateIds);
        //->orderByRaw("FIND_IN_SET(city_id,'$arCityIds')");
    }
    public function scopeStateCode($query, $stateCodes){
        
        return $query->join("qchsoft_location_states as states",
        "states.id", "=", "qchsoft_location_cities.state_id")
        ->whereIn('states.code', $stateCodes)
        ->select("qchsoft_location_cities.*");
        //->orderByRaw("FIND_IN_SET(city_id,'$arCityIds')");
    }

    public function scopeCountryId($query, $arCountryIds){
        return $query->join("qchsoft_location_countries as countries",
        "countries.id", "=", "qchsoft_location_states.country_id")
        ->join("qchsoft_location_states as states",
        "states.id", "=", "city.state_id")
        ->whereIn('country.id', $arCountryIds)->select("qchsoft_location_cities.*");
    }

}
