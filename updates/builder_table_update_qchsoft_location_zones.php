<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftLocationZones extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_location_zones', function($table)
        {
            $table->string('external_id', 150)->nullable();
            $table->integer('active')->default(0);
            $table->string('code', 255)->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->smallInteger('city_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_location_zones', function($table)
        {
            $table->dropColumn('external_id');
            $table->dropColumn('active');
            $table->string('code', 255)->nullable(false)->change();
            $table->text('description')->nullable(false)->change();
            $table->smallInteger('city_id')->nullable(false)->change();
        });
    }
}
