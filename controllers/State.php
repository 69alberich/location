<?php namespace Qchsoft\Location\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class State extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        \Backend\Behaviors\ImportExportController::class,
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $importExportConfig = 'config_import_export.yaml';
    
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qchsoft.Location', 'main-menu-item', 'side-menu-item');
    }
}
