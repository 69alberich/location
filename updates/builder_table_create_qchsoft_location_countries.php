<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftLocationCountries extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_location_countries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 150);
            $table->text('description');
            $table->string('code');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_location_countries');
    }
}
