<?php namespace Qchsoft\Location\Classes\Import;

use \Backend\Models\ImportModel;
use QchSoft\Location\Models\City;

class CitiesImport extends ImportModel {


    const EVENT_BEFORE_IMPORT = 'model.beforeImport';
    const EVENT_AFTER_IMPORT = 'model.afterImport';

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'external_id' => 'required',
        'name' => 'required'
    ];

    protected $importedData;

    public function importData($results, $sessionKey = null){

        $this->prepareImportData($results);
        
        foreach ($this->importedData as $row => $data) {
            //trace_log($data);

            try {

                $city = City::firstOrNew([
                    "external_id" => $data["external_id"],
                ]);

                if($city->exists){
                    $this->logUpdated();
                }else{
                    $this->logCreated();
                }

                $city->fill($data);

                $city->save();

                
            }
            catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }

        }
    }

    protected function prepareImportData($results){
        $this->importedData = $results;
        trace_log($results);
    }

    protected function fireBeforeImportEvent(){

        $arEventData = Event::fire(self::EVENT_BEFORE_IMPORT, $arEventData);
        trace_log($arEventData);
    }



}