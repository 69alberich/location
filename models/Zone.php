<?php namespace Qchsoft\Location\Models;

use Model;
use Qchsoft\Location\Models\City;
/**
 * Model
 */
class Zone extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_location_zones';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'city' => City::class
    ];

    public $fillable = [
        'name',
        'code',
        'city_id',
        'description',
        'external_id',
        'active',
    ];

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

    public function scopeCityId($query, $arCityIds){
        
        return $query->whereIn('city_id', $arCityIds);
        //->orderByRaw("FIND_IN_SET(city_id,'$arCityIds')");
    }

    public function scopeCityCode($query, $cityCodes){
        
        return $query->join("qchsoft_location_cities as cities",
        "cities.id", "=", "qchsoft_location_zones.city_id")
        ->whereIn('cities.code', $cityCodes)
        ->select("qchsoft_location_zones.*");
        //->orderByRaw("FIND_IN_SET(city_id,'$arCityIds')");
    }

    public function scopeStateId($query, $arStateIds){
        return $query->join("qchsoft_location_cities as cities",
        "cities.id", "=", "qchsoft_location_zones.city_id")
        ->join("qchsoft_location_states as states",
        "states.id", "=", "cities.state_id")
        ->whereIn('states.id', $arStateIds)->select("qchsoft_location_zones.*");
    }

    public function scopeActive(){
        return $query->where('active', 1);
    }
}
