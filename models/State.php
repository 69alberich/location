<?php namespace Qchsoft\Location\Models;

use Qchsoft\Location\Models\Country;
use Model;

/**
 * Model
 */
class State extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qchsoft_location_states';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'country' => Country::class
    ];

    public $fillable = [
        'name',
        'code',
        'state_id',
        'description',
        'country_id',
        'active',
        'external_id',
    ];
}
