<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftLocationCities3 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->integer('active')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->dropColumn('active');
        });
    }
}
