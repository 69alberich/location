<?php namespace Qchsoft\Location;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents(){
        return [
            'QchSoft\Location\Components\ZoneList' => 'ZoneList',
            'QchSoft\Location\Components\CityList' => 'CityList'
        ];
    }

    public function registerSettings()
    {
    }
}
