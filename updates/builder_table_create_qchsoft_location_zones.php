<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQchsoftLocationZones extends Migration
{
    public function up()
    {
        Schema::create('qchsoft_location_zones', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->text('description');
            $table->smallInteger('city_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qchsoft_location_zones');
    }
}
