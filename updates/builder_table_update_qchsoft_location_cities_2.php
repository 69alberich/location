<?php namespace Qchsoft\Location\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQchsoftLocationCities2 extends Migration
{
    public function up()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->string('code', 255)->nullable()->change();
            $table->integer('state_id')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('qchsoft_location_cities', function($table)
        {
            $table->string('code', 255)->nullable(false)->change();
            $table->integer('state_id')->nullable(false)->change();
            $table->text('description')->nullable(false)->change();
        });
    }
}
